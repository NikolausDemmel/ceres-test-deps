#!/usr/bin/env bash

set -e
set +x

rm -rf ./build
mkdir build
cd build
cmake ../ceres-solver -DCMAKE_PREFIX_PATH="../cmake/eigen3"  # -DEIGEN3_SOURCE_TREE="../eigen"
