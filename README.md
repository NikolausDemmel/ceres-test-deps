# Testing Eigen from source tree

This repo is for testing ceres with Eigen from a source tree (no cmake
build/install dir).

There are two variants:

1. [find module](cmake/modules/FindEigen3.cmake)
2. [config files](cmake/eigen3)

For license information see the corresponding files.

See also: [ceres issue tracker](https://github.com/ceres-solver/ceres-solver/commit/33dd469a53383743af00a711a6a85e64c35177e8#commitcomment-41529630)
